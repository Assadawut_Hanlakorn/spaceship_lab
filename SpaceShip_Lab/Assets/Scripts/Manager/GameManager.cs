﻿using System;
using Spaceship;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Manager
{
    public class GameManager : MonoBehaviour
    {
         public event Action OnRestarted;
         
        
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private BossSpaceship bossSpaceship;

        
        
        
        [HideInInspector]
        public PlayerSpaceship spawnedPlayerShip;
        public EnemySpaceship spawnedEnemyShip;
        
        
        [Header("Spawners")]
        public BossSpaceship spawnedBoss;
        [SerializeField] private Transform[] spawnersLevelOne;
        [SerializeField] private Transform[] spawnersLevelTwo;

        
        public static GameManager Instance { get; private set; }
        
        private void Awake()
        {
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }

        }
        
        
        internal void StartGame()
        {
            SpawnPlayerSpaceship();
            SpawnEnemySpaceshipSetOne();
        }
        
        private void SpawnPlayerSpaceship()
        {
            spawnedPlayerShip = Instantiate(playerSpaceship);
            spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
        }

       
        
        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }
        
        private void SpawnEnemySpaceshipSetOne()
        {
            foreach (var unused in spawnersLevelOne)
            {
                
                 var spawnPoint = new Vector2(Random.Range(-7f, 7f), Random.Range(1f, 4f));
                 spawnedEnemyShip = Instantiate(enemySpaceship, spawnPoint, Quaternion.identity);
                 spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;
                 
            }
            
        }
        internal void SpawnEnemySpaceshipSetTwo()
        {
            foreach (var unused in spawnersLevelTwo)
            {
                var spawnPoint = new Vector2(Random.Range(-7f, 7f), Random.Range(1f, 4f));
                spawnedEnemyShip = Instantiate(enemySpaceship, spawnPoint, Quaternion.identity);
                spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;
            }
        }

        internal void SpawnBoss()
        {
            spawnedBoss = Instantiate(bossSpaceship, bossSpaceship.transform.position, Quaternion.identity);
            spawnedBoss.OnExploded += OnEnemySpaceshipExploded;
        }
        
        private void OnEnemySpaceshipExploded()
        {
            ScoreManager.Instance.UpdateScore(1);
            
        }
        
        internal void Restart()
        {
            OnRestarted?.Invoke();
            DestroyRemainingShips();
        }

        internal void Quit()
        {
            Application.Quit();
        }

        private void DestroyRemainingShips()
        {
            var remainingEnemy = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemy)
            {
                Destroy(enemy);
            }
            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }            
        }
    }
}
