﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        public event Action OnScoreUpdated;

        private int playerScore;

        public static ScoreManager Instance { get; private set; }
    
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);                
            }
        }
        
        private void Start()
        {
            playerScore = 0;
            GameManager.Instance.OnRestarted += OnRestarted;
        }

        public int GetScore()
        {
            return playerScore;
        }
        
        public void UpdateScore(int score)
        {
            playerScore += score;
            OnScoreUpdated?.Invoke();
            NextLevel();
        }

        private void ResetScore()
        {
            playerScore = 0;
            //OnScoreUpdated?.Invoke();
        }
        
        private void OnRestarted()
        {
            ResetScore();
        }

        private void NextLevel()
        {
            if (playerScore == 4)
            {
                GameManager.Instance.SpawnEnemySpaceshipSetTwo();
            }
            else if (playerScore == 9)
            {
                GameManager.Instance.SpawnBoss();
            }
            else if (playerScore == 10)
            {
                GameManager.Instance.Restart();
            }
            
        }
       
    }
}


