using System;
using Manager;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Spaceship
{
    public class BossSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        
        [SerializeField] private int bossSpaceshipHp;
        
        [SerializeField] private double bossFireRate = 0.5;
        private float fireCounter = 0f;
        
        [SerializeField] private Transform[] gunSpreadPosition ;
        [SerializeField] private Bullet spreadBullet;
       
        private void Awake()
        {
            Debug.Assert(bossFireRate > 0, "enemyFireRate has to be more than zero");            
        }
        
        
        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= bossFireRate)
            {
             foreach (var gunPoint in gunSpreadPosition)
             {
                var bullet = Instantiate(spreadBullet, gunPoint.position , Quaternion.identity);
                bullet.Init(Vector2.down);
                SoundManager.Instance.Play(SoundManager.Sound.EnemyFire); 
              }
             fireCounter = 0;
            }
        }

        
        public void TakeHit(int damage)
        {
            bossSpaceshipHp -= damage;
            if (bossSpaceshipHp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.Play(SoundManager.Sound.EnemyExplode);
            Debug.Assert(bossSpaceshipHp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}
