using System;
using Manager;
using UnityEngine;
namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] internal int enemySpaceshipMoveSpeed;
        

        [SerializeField] private double enemyFireRate = 0.5;
        private float fireCounter = 0f;
        
       
        
        
        public void TakeHit(int damage)
        {
            enemySpaceshipHp -= damage;

            if (enemySpaceshipHp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.Play(SoundManager.Sound.EnemyExplode);
            Debug.Assert(enemySpaceshipHp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= enemyFireRate)
            {
                SoundManager.Instance.Play(SoundManager.Sound.EnemyFire);
                var bullet = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.EnemyBullet);
                if (bullet)
                {
                    bullet.transform.position = gunPosition.position;
                    bullet.transform.rotation = Quaternion.identity;
                    bullet.SetActive(true);
                    bullet.GetComponent<Bullet>().Init(Vector2.down);    
                }
                
                fireCounter = 0;
            }
        }
    }
}