using System;
using Manager;
using Player;
using TMPro;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        
        [SerializeField] protected int playerSpaceshipHp;
        [SerializeField] internal int playerSpaceshipMoveSpeed;
        
        
        [Header("Mechanic")]
        [SerializeField] private Bullet bigBullet;
        [SerializeField] private Bullet spreadBullet;
        [SerializeField] private Transform[] gunSpreadPosition;
        
        
        public override void Fire()
        {
            SoundManager.Instance.Play(SoundManager.Sound.PlayerFire);
            var bullet = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.PlayerBullet);
            if (bullet)
            {
                bullet.transform.position = gunPosition.position;
                bullet.transform.rotation = Quaternion.identity;
                bullet.SetActive(true);
                bullet.GetComponent<Bullet>().Init(Vector2.up);
            }
        }
        
         public void SpreadFire()
         {
             foreach (var gunPoint in gunSpreadPosition)
             {
                 SoundManager.Instance.Play(SoundManager.Sound.SpreadFire);
                 var bullet = Instantiate(spreadBullet, gunPoint.position, Quaternion.identity);
                 bullet.Init(Vector2.up);
                 
             }
         }
                
         public void BigBullet()
         {
             SoundManager.Instance.Play(SoundManager.Sound.BigFire);
             var bullet = Instantiate(bigBullet, gunPosition.position, Quaternion.identity);
             bullet.Init(Vector2.up);
             
         }
        
        
        public void TakeHit(int damage)
        {
            playerSpaceshipHp -= damage;
            if (playerSpaceshipHp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.Play(SoundManager.Sound.PlayerExplode);
            Debug.Assert(playerSpaceshipHp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public void GetHeal(int hp)
        {
            /*if (playerSpaceshipHp < 100)
            {
                playerSpaceshipHp += hp;
            }
            else
            {
                playerSpaceshipHp += 0;
            }*/
        }
    }
}